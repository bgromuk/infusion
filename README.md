Spring Boot service to validate trades.

To run write 'mvn spring-boot:run' in console. 

Simply put json with trades to body and send it to:

http://localhost:8080/trades/ALL

As answer you will get 400 Bad request response with error code and description. Also in "arguments" field of error you have trade that has error.
In case of valid trades array - service will return it without changes.