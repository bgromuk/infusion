package com.infusion.testtask.beans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "customer",
        "ccyPair",
        "type",
        "direction",
        "tradeDate",
        "amount1",
        "amount2",
        "rate",
        "valueDate",
        "legalEntity",
        "trader",
        "style",
        "strategy",
        "deliveryDate",
        "expiryDate",
        "payCcy",
        "premium",
        "premiumCcy",
        "premiumType",
        "premiumDate",
        "excerciseDate"
})
public class Trade {

    @JsonProperty("customer")
    private String customer;
    @JsonProperty("ccyPair")
    private String ccyPair;
    @JsonProperty("type")
    private String type;
    @JsonProperty("direction")
    private String direction;
    @JsonProperty("tradeDate")
    private Date tradeDate;
    @JsonProperty("amount1")
    private Double amount1;
    @JsonProperty("amount2")
    private Double amount2;
    @JsonProperty("rate")
    private Double rate;
    @JsonProperty("valueDate")
    private Date valueDate;
    @JsonProperty("legalEntity")
    private String legalEntity;
    @JsonProperty("trader")
    private String trader;
    @JsonProperty("style")
    private String style;
    @JsonProperty("strategy")
    private String strategy;
    @JsonProperty("deliveryDate")
    private Date deliveryDate;
    @JsonProperty("expiryDate")
    private Date expiryDate;
    @JsonProperty("payCcy")
    private String payCcy;
    @JsonProperty("premium")
    private Double premium;
    @JsonProperty("premiumCcy")
    private String premiumCcy;
    @JsonProperty("premiumType")
    private String premiumType;
    @JsonProperty("premiumDate")
    private Date premiumDate;
    @JsonProperty("excerciseDate")
    private Date excerciseDate;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("customer")
    public String getCustomer() {
        return customer;
    }

    @JsonProperty("customer")
    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @JsonProperty("ccyPair")
    public String getCcyPair() {
        return ccyPair;
    }

    @JsonProperty("ccyPair")
    public void setCcyPair(String ccyPair) {
        this.ccyPair = ccyPair;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("direction")
    public String getDirection() {
        return direction;
    }

    @JsonProperty("direction")
    public void setDirection(String direction) {
        this.direction = direction;
    }

    @JsonProperty("tradeDate")
    public Date getTradeDate() {
        return tradeDate;
    }

    @JsonProperty("tradeDate")
    public void setTradeDate(Date tradeDate) {
        this.tradeDate = tradeDate;
    }

    @JsonProperty("amount1")
    public Double getAmount1() {
        return amount1;
    }

    @JsonProperty("amount1")
    public void setAmount1(Double amount1) {
        this.amount1 = amount1;
    }

    @JsonProperty("amount2")
    public Double getAmount2() {
        return amount2;
    }

    @JsonProperty("amount2")
    public void setAmount2(Double amount2) {
        this.amount2 = amount2;
    }

    @JsonProperty("rate")
    public Double getRate() {
        return rate;
    }

    @JsonProperty("rate")
    public void setRate(Double rate) {
        this.rate = rate;
    }

    @JsonProperty("valueDate")
    public Date getValueDate() {
        return valueDate;
    }

    @JsonProperty("valueDate")
    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    @JsonProperty("legalEntity")
    public String getLegalEntity() {
        return legalEntity;
    }

    @JsonProperty("legalEntity")
    public void setLegalEntity(String legalEntity) {
        this.legalEntity = legalEntity;
    }

    @JsonProperty("trader")
    public String getTrader() {
        return trader;
    }

    @JsonProperty("trader")
    public void setTrader(String trader) {
        this.trader = trader;
    }

    @JsonProperty("style")
    public String getStyle() {
        return style;
    }

    @JsonProperty("style")
    public void setStyle(String style) {
        this.style = style;
    }

    @JsonProperty("strategy")
    public String getStrategy() {
        return strategy;
    }

    @JsonProperty("strategy")
    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    @JsonProperty("deliveryDate")
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    @JsonProperty("deliveryDate")
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @JsonProperty("expiryDate")
    public Date getExpiryDate() {
        return expiryDate;
    }

    @JsonProperty("expiryDate")
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @JsonProperty("payCcy")
    public String getPayCcy() {
        return payCcy;
    }

    @JsonProperty("payCcy")
    public void setPayCcy(String payCcy) {
        this.payCcy = payCcy;
    }

    @JsonProperty("premium")
    public Double getPremium() {
        return premium;
    }

    @JsonProperty("premium")
    public void setPremium(Double premium) {
        this.premium = premium;
    }

    @JsonProperty("premiumCcy")
    public String getPremiumCcy() {
        return premiumCcy;
    }

    @JsonProperty("premiumCcy")
    public void setPremiumCcy(String premiumCcy) {
        this.premiumCcy = premiumCcy;
    }

    @JsonProperty("premiumType")
    public String getPremiumType() {
        return premiumType;
    }

    @JsonProperty("premiumType")
    public void setPremiumType(String premiumType) {
        this.premiumType = premiumType;
    }

    @JsonProperty("premiumDate")
    public Date getPremiumDate() {
        return premiumDate;
    }

    @JsonProperty("premiumDate")
    public void setPremiumDate(Date premiumDate) {
        this.premiumDate = premiumDate;
    }

    @JsonProperty("excerciseDate")
    public Date getExcerciseDate() {
        return excerciseDate;
    }

    @JsonProperty("excerciseDate")
    public void setExcerciseDate(Date excerciseDate) {
        this.excerciseDate = excerciseDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Trade{" +
                "customer='" + customer + '\'' +
                ", ccyPair='" + ccyPair + '\'' +
                ", type='" + type + '\'' +
                ", direction='" + direction + '\'' +
                ", tradeDate=" + tradeDate +
                ", amount1=" + amount1 +
                ", amount2=" + amount2 +
                ", rate=" + rate +
                ", valueDate=" + valueDate +
                ", legalEntity='" + legalEntity + '\'' +
                ", trader='" + trader + '\'' +
                ", style='" + style + '\'' +
                ", strategy='" + strategy + '\'' +
                ", deliveryDate=" + deliveryDate +
                ", expiryDate=" + expiryDate +
                ", payCcy='" + payCcy + '\'' +
                ", premium=" + premium +
                ", premiumCcy='" + premiumCcy + '\'' +
                ", premiumType='" + premiumType + '\'' +
                ", premiumDate=" + premiumDate +
                ", excerciseDate=" + excerciseDate +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
