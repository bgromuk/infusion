package com.infusion.testtask.service.filter;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.chains.ClientChain;
import com.infusion.testtask.service.chains.CurrencyChain;
import com.infusion.testtask.service.chains.DateChain;
import com.infusion.testtask.service.chains.FilterChain;
import org.springframework.validation.Errors;

import java.util.stream.Stream;

/**
 * Created by rabbit on 09.06.17.
 */
public class AllButch extends FilterButch {
    public AllButch(Stream<Trade> trades, Errors errors) throws Exception {
        super(trades, errors);
    }

    @Override
    protected FilterChain initChains() {
        FilterChain clientChain = new ClientChain();
        DateChain dateChain = new DateChain();
        clientChain.setNextChain(dateChain);
        CurrencyChain currencyChain = new CurrencyChain();
        dateChain.setNextChain(currencyChain);
        return clientChain;
    }

    @Override
    protected Stream<Trade> filter(Stream<Trade> tradesToFilter) {
        return tradesToFilter;
    }
}
