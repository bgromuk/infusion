package com.infusion.testtask.service.filter;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.chains.FilterChain;
import org.springframework.validation.Errors;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by rabbit on 09.06.17.
 */
public abstract class FilterButch {
    private Stream<Trade> currencyStream;
    private FilterChain c1;
    private Errors errors;


    public FilterButch(Stream<Trade> trades, Errors errors) throws Exception {
        if (trades == null) {
            throw new Exception("Stream is empty");
        }
        this.currencyStream = filter(trades);
        if (this.currencyStream == null) {
            throw new Exception("Chain is empty");
        }
        this.errors = errors;
        this.c1 = initChains();
        if (this.c1 == null) {
            throw new Exception("Chain is empty");
        }
    }

    public void perform() {
        currencyStream.forEach(tr -> {
            c1.doFilter(tr, errors);
        });
    }

    protected abstract FilterChain initChains();
    protected abstract Stream<Trade> filter(Stream<Trade> tradesToFilter);
}
