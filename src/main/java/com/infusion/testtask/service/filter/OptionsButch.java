package com.infusion.testtask.service.filter;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.chains.ClientChain;
import com.infusion.testtask.service.chains.CurrencyChain;
import com.infusion.testtask.service.chains.DateChain;
import com.infusion.testtask.service.chains.FilterChain;
import com.infusion.testtask.service.chains.options.AmericanOptionsChain;
import com.infusion.testtask.service.chains.options.OptionsDateChain;
import com.infusion.testtask.service.chains.options.OptionsStyle;
import com.infusion.testtask.service.chains.options.OptionsStyleChain;
import org.springframework.validation.Errors;

import java.util.stream.Stream;

/**
 * Created by rabbit on 09.06.17.
 */
public class OptionsButch extends FilterButch {

    public static final String OPTION = "VanillaOption";

    public OptionsButch(Stream<Trade> trades, Errors errors) throws Exception {
        super(trades, errors);
    }

    @Override
    protected FilterChain initChains() {
        OptionsStyleChain optionsStyle = new OptionsStyleChain();
        OptionsDateChain optionsDateChain = new OptionsDateChain();
        AmericanOptionsChain americanOptionsChain = new AmericanOptionsChain();

        optionsStyle.setNextChain(optionsDateChain);
        optionsDateChain.setNextChain(americanOptionsChain);
        return optionsStyle;
    }

    @Override
    protected Stream<Trade> filter(Stream<Trade> tradesToFilter) {
        return tradesToFilter.filter(p -> {
            if (OPTION.equals(p.getType())) {
                return true;
            }
            return false;
        });
    }
}
