package com.infusion.testtask.service.chains.options;

/**
 * Created by rabbit on 11.06.17.
 */
public interface OptionsStyle {
    String EUROPEAN = "EUROPEAN";
    String AMERICAN = "AMERICAN";
}
