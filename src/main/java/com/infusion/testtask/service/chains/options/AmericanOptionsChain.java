package com.infusion.testtask.service.chains.options;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.chains.AbstrFilterChain;
import com.infusion.testtask.service.chains.options.OptionsStyle;
import org.springframework.validation.Errors;

import java.util.Date;

/**
 * Created by rabbit on 10.06.17.
 */
public class AmericanOptionsChain extends AbstrFilterChain {

    public static final String EXERCISEDTNONULL = "EXERCISEDTNONULL";
    public static final String EXERCISEDTNONULL_DESCR = "Excercise date must be not null in case of AMERICAN options style";


    public static final String EXERAFTERTRDATE = "EXERAFTERTRDATE";
    public static final String EXERAFTERTRDATE_DESCR = "Excercise date must be after trade date in case of AMERICAN options style";

    public static final String EXERBEFOREEXPDT = "EXERBEFOREEXPDT";
    public static final String EXERBEFOREEXPDT_DESCR = "Excercise date must be before expiry date in case of AMERICAN options style";

    @Override
    public void perform(Trade curTrade, Errors errors) {
        String[] curTradeStr = {curTrade.toString()};
        if (curTrade.getStyle().equals(OptionsStyle.AMERICAN)) {

            Date excerciseDate = curTrade.getExcerciseDate();
            if (excerciseDate == null) {
                errors.reject(EXERCISEDTNONULL, curTradeStr, EXERCISEDTNONULL_DESCR);
            }
            else
            {
                Date tradeDate = curTrade.getTradeDate();
                if (tradeDate == null ||
                        !excerciseDate.after(tradeDate)) {
                    errors.reject(EXERAFTERTRDATE, curTradeStr, EXERAFTERTRDATE_DESCR);
                }

                Date expDate = curTrade.getExpiryDate();
                if (expDate == null ||
                        !excerciseDate.before(expDate)) {
                    errors.reject(EXERBEFOREEXPDT, curTradeStr, EXERBEFOREEXPDT_DESCR);
                }
            }
        }
    }
}
