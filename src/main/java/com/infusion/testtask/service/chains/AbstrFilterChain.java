package com.infusion.testtask.service.chains;

import com.infusion.testtask.beans.Trade;
import org.springframework.validation.Errors;

/**
 * Created by rabbit on 10.06.17.
 */
public abstract class AbstrFilterChain implements FilterChain {
    private FilterChain chain;

    @Override
    public void setNextChain(FilterChain nextChain) {
        this.chain=nextChain;
    }

    @Override
    public void doFilter(Trade curTrade, Errors errors) {
        perform(curTrade, errors);
        if (chain != null) {
            chain.doFilter(curTrade, errors);
        }
    }

    public abstract void perform(Trade curTrade, Errors errors);
}
