package com.infusion.testtask.service.chains.options;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.chains.AbstrFilterChain;
import org.springframework.validation.Errors;

/**
 * Created by rabbit on 10.06.17.
 */
public class OptionsStyleChain extends AbstrFilterChain implements OptionsStyle {

    public static final String OPTSTNOTVALID = "OPTSTNOTVALID";
    public static final String OPTSTNOTVALID_DESCR = "Options style can be American or European";

    @Override
    public void perform(Trade curTrade, Errors errors) {
        String[] curTradeStr = {curTrade.toString()};

        if (curTrade.getStyle() == null
                || curTrade.getStyle() == EUROPEAN || curTrade.getStyle() == AMERICAN) {
            errors.reject(OPTSTNOTVALID, curTradeStr, OPTSTNOTVALID_DESCR);

        }
    }
}
