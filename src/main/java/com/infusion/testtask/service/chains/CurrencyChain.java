package com.infusion.testtask.service.chains;

import com.infusion.testtask.beans.Trade;
import org.springframework.validation.Errors;

import java.util.Calendar;
import java.util.Currency;
import java.util.Date;

/**
 * Created by rabbit on 10.06.17.
 */
public class CurrencyChain extends AbstrFilterChain {

    public static final String CURCODENOTVALID = "CURCODENOTVALID";
    public static final String CURCODENOTVALID_DESCR = "Currency code is not valid ISO 4217";

    @Override
    public void perform(Trade curTrade, Errors errors) {
        String[] curTradeStr = {curTrade.toString()};

        if (curTrade.getPayCcy() != null && curTrade.getPremiumCcy() != null) {
            try {
                Currency.getInstance(curTrade.getPremiumCcy());
                Currency.getInstance(curTrade.getPayCcy());
            } catch (IllegalArgumentException ia) {
                errors.reject(CURCODENOTVALID, curTradeStr, CURCODENOTVALID_DESCR);
            }

        }
    }
}
