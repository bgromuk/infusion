package com.infusion.testtask.service.chains;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.filter.AllButch;
import com.infusion.testtask.service.filter.OptionsButch;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;

public class AllValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    public void validate(Object target, Errors e) {
        List<Trade> trades = (List<Trade>) target;

        try {
            AllButch allButch = new AllButch(trades.stream(), e);
            allButch.perform();

            OptionsButch optButch = new OptionsButch(trades.stream(), e);
            optButch.perform();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

    }
}
