package com.infusion.testtask.service.chains;

import com.infusion.testtask.beans.Trade;
import org.springframework.validation.Errors;

import java.util.Currency;

/**
 * Created by rabbit on 10.06.17.
 */
public class ClientChain extends AbstrFilterChain {

    public static final String JUPITER_1 = "JUPITER1";
    public static final String JUPITER_2 = "JUPITER2";
    public static final String CUSTNOTSUPP = "CUSTNOTSUPP";
    public static final String CUSTNOTSUPP_DESCR = "Customer not supported, only JUPITER1 and JUPITER2 are";

    @Override
    public void perform(Trade curTrade, Errors errors) {
        String[] curTradeStr = {curTrade.toString()};

        if (curTrade.getCustomer() != null &&
                !curTrade.getCustomer().equals(JUPITER_1) && !curTrade.getCustomer().equals(JUPITER_2)) {
            errors.reject(CUSTNOTSUPP, curTradeStr, CUSTNOTSUPP_DESCR);
        }
    }
}
