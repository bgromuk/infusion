package com.infusion.testtask.service.chains;

import com.infusion.testtask.beans.Trade;
import org.springframework.validation.Errors;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by rabbit on 10.06.17.
 */
public class DateChain extends AbstrFilterChain {

    public static final String VDATEWEEKEND = "VDATEWEEKEND";
    public static final String VDATEWEEKEND_DESCR = "Value date cant be weekend days";
    public static final String VDATEBEFTRDATE = "VDATEBEFTRDATE";
    public static final String VDATEBEFTRDATE_DESCR = "Value date cant be before trade date";

    @Override
    public void perform(Trade curTrade, Errors errors) {
        String[] curTradeStr = {curTrade.toString()};
        Date valDate = curTrade.getValueDate();

        if (valDate != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(valDate);

            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {
                errors.reject(VDATEWEEKEND, curTradeStr, VDATEWEEKEND_DESCR);
            }
            if (valDate.before(curTrade.getTradeDate())) {
                errors.reject(VDATEBEFTRDATE, curTradeStr, VDATEBEFTRDATE_DESCR);
            }
        }
    }
}
