package com.infusion.testtask.service.chains;

import com.infusion.testtask.beans.Trade;
import org.springframework.validation.Errors;

import java.util.stream.Stream;

/**
 * Created by rabbit on 09.06.17.
 */
public interface FilterChain {
    void setNextChain(FilterChain nextChain);

    void doFilter(Trade curTrade, Errors errors);
}
