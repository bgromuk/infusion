package com.infusion.testtask.service.chains.options;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.chains.AbstrFilterChain;
import org.springframework.validation.Errors;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by rabbit on 10.06.17.
 */
public class OptionsDateChain extends AbstrFilterChain {

    public static final String EXPDTBEFDELDT = "EXPDTBEFDELDT";
    public static final String EXPDTBEFDELDT_DESCR = "Expiry date must be before delivery date in case of OPTIONS";

    public static final String PREMDTBEFDELDT = "PREMDTBEFDELDT";
    public static final String PREMDTBEFDELDT_DESCR = "Premium date must be before delivery date in case of OPTIONS";

    @Override
    public void perform(Trade curTrade, Errors errors) {
        String[] curTradeStr = {curTrade.toString()};
        Date deliveryDate = curTrade.getDeliveryDate();
        if (deliveryDate != null) {
            Date expDate = curTrade.getExpiryDate();
            if (expDate == null ||
                    !expDate.before(deliveryDate)) {
                errors.reject(EXPDTBEFDELDT, curTradeStr, EXPDTBEFDELDT_DESCR);
            }

            Date premiumDate = curTrade.getPremiumDate();
            if (premiumDate == null ||
                    !premiumDate.before(deliveryDate)) {
                errors.reject(PREMDTBEFDELDT, curTradeStr, PREMDTBEFDELDT_DESCR);
            }
        }

    }
}
