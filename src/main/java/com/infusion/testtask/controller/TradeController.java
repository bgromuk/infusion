package com.infusion.testtask.controller;

import com.infusion.testtask.beans.Trade;
import com.infusion.testtask.service.chains.AllValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rabbit on 07.06.17.
 */
@RestController
@RequestMapping("/trades")
public class TradeController {

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new AllValidator());
    }


    @RequestMapping(value="/ALL", method = RequestMethod.POST)
    public ResponseEntity<List<Trade>> validateAll(@Valid @RequestBody List<Trade> inpTrades) {
        return new ResponseEntity<List<Trade>>(inpTrades, HttpStatus.OK);

    }
}